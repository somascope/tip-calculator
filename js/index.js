new Vue({
  el: '#app',
  data() {
    return {
      subtotal: '100',
      tipPercent: 15,
      tipMin: 0,
      tipMax: 30,
      total: '',
      splitMin: 2,
      splitMax: 15,
      splitShow: false,
      splitNumber: 2,
      splitTextArr: [
        `Yes`,
        `No`
      ],
      splitGlyphArr: [
        'person',
        'people'
      ]
    }
  },
  computed: {
    tipAmount() {
      return (this.subtotal * (this.tipPercent / 100)).toFixed(2);
    },
    splitLabel: function () {
      return this.splitShow ? this.splitTextArr[0] : this.splitTextArr[1]
    },
    splitGlyph: function () {
      return this.splitShow ? this.splitGlyphArr[0] : this.splitGlyphArr[1]
    },
    splitResult () {
      let num = this.total / this.splitNumber;
      return (Math.floor(num * 100) / 100).toFixed(2);
    }
  },
  methods: {
    calcTotal() {
      this.total = (Number(this.subtotal) + Number(this.tipAmount)).toFixed(2);
      if (window.localStorage) {
        localStorage.setItem('subtotal', this.subtotal);
        localStorage.setItem('tipPercent', this.tipPercent);
      }
    },
    handleSplit () {
      if (window.localStorage) {
        localStorage.setItem('splitShow', this.splitShow);
      }
    },
    resetTip () {
      this.tipPercent = 15;
    },
    resetSplit () {
      this.splitNumber = 2;
    }
  },
  filters: {
    commas (value) {
      return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
  },
  watch: {
    splitNumber () {
      if (window.localStorage) {
        localStorage.setItem('splitNumber', this.splitNumber);
      }
    },
    splitShow () {
      this.handleSplit();
    },
    subtotal () {
      this.calcTotal();
    },
    tipPercent () {
      this.calcTotal();
    }
  },
  mounted () {
    if (window.localStorage) {
      this.subtotal = localStorage.getItem('subtotal');
      if (this.subtotal == null) { this.subtotal = '100' }
      
      this.tipPercent = localStorage.getItem('tipPercent');
      if (this.tipPercent == null) { this.tipPercent = 15 }
      
      this.splitShow = localStorage.getItem('splitShow');
      if (this.splitShow == null) { this.splitShow = false }
      
      this.splitNumber = localStorage.getItem('splitNumber');
      if (this.splitNumber == null) { this.splitNumber = 2 }
    }
    this.calcTotal();
  }
})